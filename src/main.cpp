#include <Arduino.h>
#include <Arduino_GFX.h>
#include <Arduino_GFX_Library.h>
#include <BluetoothSerial.h>

// GH_MODULES //

#include <GH_System.h>
#include <GH_Display.h>
#include <GH_ESP32BT.h>
#include <GH_Player.h>

Player player_1 = Player(0, 0, 10, 10, WHITE, 20, 1, 20, 200, 280, 20);
//(_x_coordinate, _y_coordinate, _wide, _high, _colour, _speed, _move_control,
// x_right_limit, x_left_limit, y_up_limit, y_down_limit);

void setup(void) {
  GH_Display::init();
  GH_Display::fillScreen(BLACK);
  GH_esp32BT::init();
  GH_Display::fillRect(0, 0, 20, 20, WHITE);
}

void loop() {
  player_1.move();

  delay(100);
}
