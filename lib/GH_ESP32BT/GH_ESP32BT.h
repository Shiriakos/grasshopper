/**
 * Cool Technologies Module - BT Level meter for water tanks
 *
 * @file GH_ESP32BT.h
 * @author Shiris S.G.
 * @version 0.1
 * @date 2022/02/08
 * @brief Module for BlueTooth communication via ESP32.
 *
 * All rights reserved.
 *
 */

#include <Arduino.h>
#include "BluetoothSerial.h"

#include <GH_System.h>

#ifndef _GH_ESP32BT_
#define _GH_ESP32BT_

#pragma once

namespace GH_esp32BT {

void init();
void write(uint8_t messageValue);
int read(void);
int available(void);

}  // namespace GH_esp32BT

#endif
