/**
 * Cool Technologies Module - BT Level meter for water tanks
 *
 * @file GH_ESP32BT.cpp
 * @author Shiris S.G.
 * @version 0.1
 * @date 2022/02/08
 * @brief Module for BlueTooth communication via ESP32.
 *
 * All rights reserved.
 *
 */

#include <GH_ESP32BT.h>

BluetoothSerial GH_ESP32_BT;

namespace GH_esp32BT {

/**
 *
 * @fn void init()
 * @author Shiris S.G.
 * @date 2022/02/08
 * @brief Procedure. It turns on the Blueetooth signal of ESP32 and it sets
 * device name. GH_ESP32_BT.begin("name");
 *
 * All rights reserved.
 *
 */

void init() {
#if DEBUG_MODE_BT == 1
  Serial.println("-------------------------------------------");
  Serial.println("Start GH_esp32BT::Init()");
#endif

  GH_ESP32_BT.begin("GRASSHOPPER_v_0_3");  // Name of your Bluetooth interface
  // -> will show up on your phone

#if DEBUG_MODE_BT == 1
  Serial.println("BT Name: GH_A");
  Serial.println("Finish GH_esp32BT::Init()");
#endif
}

/**
 *
 * @fn void write()
 * @author Shiris S.G.
 * @date 2022/02/08
 * @brief Procedure. It sends via Blueetooth an uint8_t value.
 * GH_ESP32_BT.write(messageValue);
 *
 * All rights reserved.
 *
 */

void write(uint8_t messageValue) {
#if DEBUG_MODE_BT == 1
  Serial.println("-------------------------------------------");
  Serial.println("Start GH_esp32BT::write(messageValue)");
  Serial.print("Message to send: ");
  Serial.println(messageValue);
#endif

  GH_ESP32_BT.write(messageValue);

#if DEBUG_MODE_BT == 1

  Serial.println("Message sent.");
  Serial.println("Exit GH_esp32BT::write(messageValue)");
#endif
}

/**
 *
 * @fn int available();
 * @author Shiris S.G.
 * @date 2022/02/08
 * @brief Procedure. It checks via Blueetooth if there is a message waiting.
 *
 * All rights reserved.
 *
 */

int available() {
#if DEBUG_MODE_BT == 1
  Serial.println("-------------------------------------------");
  Serial.println("Start GH_esp32BT::available()");
#endif

  int value = GH_ESP32_BT.available();

#if DEBUG_MODE_BT == 1
  Serial.print("Messages available:");
  Serial.println(value);

#endif

  return value;
}

/**
 *
 * @fn int available();
 * @author Shiris S.G.
 * @date 2022/02/08
 * @brief Procedure. It reads via Blueetooth the next message.
 *
 * All rights reserved.
 *
 */

int read(void) {
#if DEBUG_MODE_BT == 1
  Serial.println("-------------------------------------------");
  Serial.println("Start GH_esp32BT::read()");
  Serial.print("Message recieved: ");
  Serial.print(GH_ESP32_BT.read());

#endif

  return GH_ESP32_BT.read();
}

}  // namespace GH_esp32BT
