/**
 * GRASSHOPPER Module - Display
 *
 * @file GH_Display.h
 * @author Shiris S.G.
 * @version 0.1
 * @date 2022/02/08
 * @brief Module for Display 320x240 SPI (ILI9341)
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the tearms of the GNU General Public License as published by the Free
 * Software Fundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 */

#include <Arduino.h>
#include <Arduino_GFX.h>
#include <Arduino_GFX_Library.h>

#include <GH_System.h>

#ifndef _GH_DISPLAY_
#define _GH_DISPLAY_

#pragma once

namespace GH_Display {

void init();
void fillScreen(int colour);
void fillRect(int x, int y, int w, int h, int colour);

}  // namespace GH_Display

#endif
