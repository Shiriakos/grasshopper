#include <GH_Display.h>

/**
 * GRASSHOPPER Module - Display
 *
 * @file GH_Display.cpp
 * @author Shiris S.G.
 * @version 0.1
 * @date 2022/02/08
 * @brief Module for Display 320x240 SPI (ILI9341)
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the tearms of the GNU General Public License as published by the Free
 * Software Fundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 */

// SCREEN CONFIGURATION //

Arduino_ESP32SPI bus =
    Arduino_ESP32SPI(TFT_DC, TFT_CS, TFT_SCK, TFT_MOSI, TFT_MISO);

Arduino_ILI9341 display = Arduino_ILI9341(&bus, TFT_RESET);

namespace GH_Display {

/**
 *
 * @fn void init()
 * @author Shiris S.G.
 * @date 2022/02/08
 * @brief Procedure. Init Screen.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the tearms of the GNU General Public License as published by the Free
 * Software Fundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 */

/**
 *
 * @fn void init()
 * @author Shiris S.G.
 * @date 2022/02/08
 * @brief Procedure to start the screen.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the tearms of the GNU General Public License as published by the Free
 * Software Fundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 */

void init() {
#if DEBUG_MODE_DISPLAY == 1
  Serial.println("-------------------------------------------");
  Serial.println("Start GH_Display::Init()");
#endif

  display.begin();

#if DEBUG_MODE_DISPLAY == 1

  Serial.println("Finish GH_Display::Init()");
#endif
}

/**
 *
 * @fn void fillscreen(int colour)
 * @author Shiris S.G.
 * @date 2022/02/08
 * @brief Procedure to set screen colour.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the tearms of the GNU General Public License as published by the Free
 * Software Fundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 */

void fillScreen(int colour) {
#if DEBUG_MODE_DISPLAY == 1
  Serial.println("-------------------------------------------");
  Serial.println("GH_Display::display.fillScreen()");
#endif

  display.fillScreen(colour);

#if DEBUG_MODE_DISPLAY == 1
  Serial.println("Finish GH_Display::fillscreen()");
#endif
}


/**
 *
 * @fn void fillscreen(int colour)
 * @author Shiris S.G.
 * @date 2022/02/08
 * @brief Procedure to draw a rect.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the tearms of the GNU General Public License as published by the Free
 * Software Fundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 */

void fillRect(int x, int y, int w, int h, int colour) {
#if DEBUG_MODE_DISPLAY == 1
  Serial.println("-------------------------------------------");
  Serial.println("GH_Display::display.fillScreen()");
#endif

  display.fillRect(x, y, w, h, colour);

#if DEBUG_MODE_DISPLAY == 1
  Serial.println("Finish GH_Display::fillscreen()");
#endif
}

}  // namespace GH_Display
