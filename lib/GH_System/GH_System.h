/**
 * GRASSHOPPER system configuration
 *
 * @file GH_System.h
 * @author Shiris S.G.
 * @version 0.2
 * @date 2022/02/08
 * @brief System configuration of GRASSHOPPER Devices.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the tearms of the GNU General Public License as published by the Free
 * Software Fundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 */
#include <Arduino.h>

#ifndef _GH_SYSTEM_
#define _GH_SYSTEM_


// COMMON MACROS //

#define DEEP_SLEEP 0
#define LOW_BATTERY 1
#define NORMAL 2

#define ON 1
#define OFF 0

// DEBUGS MACROS //

#define DEBUG_MODE_DISPLAY 0
#define DEBUG_MODE_BT 0
#define DEBUG_SERIAL 0
#define DEBUG_MODE_OPMODES 0

// SYSTEM STRUGHS //

struct GH_system {
  uint8_t opMode = 2;  // 0 = Deep Sleep , 1 = Low Battery , 2 = Normal
  uint16_t waitTime = 1000;
  uint8_t RX_BT_message = 0;
  uint8_t water_sensors_status[4] = {
      0, 0, 0, 0}; /*!< Each position of the array is a sensor. Value of the
                      position is its status: "0" (OFF) or "1" (ON).*/
  uint8_t water_readingValue = 0;
};

extern GH_system SystemMain;

// MODULE FUNGHIONS //

void GH_setMode(uint8_t MODE);

#endif
