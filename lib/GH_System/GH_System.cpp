/**
 * GRASSHOPPER Module - BT Level meter for water tanks
 *
 * @struct GH_system
 * @author Shiris S.G.
 * @date 2022/02/08
 * @brief Struct. It saves all settings of the GH_System.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the tearms of the GNU General Public License as published by the Free
 * Software Fundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 */

#include <GH_System.h>

struct GH_system SystemMain;

// MODULE FUNCTIONS //

/**
 *
 * @fn void GH_setMode(uint8_t MODE)
 * @author Shiris S.G.
 * @date 2022/02/08
 * @brief Procedure. Set the operational mode of device.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the tearms of the GNU General Public License as published by the Free
 * Software Fundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 */

void GH_setMode(uint8_t MODE) {
#if DEBUG_MODE_OPMODES == 1
  Serial.println("In GH_setModes(MODE)");
#endif

  switch (MODE) {
    case 0:
      SystemMain.opMode = 0;
      SystemMain.waitTime = 5000;
#if DEBUG_MODE_OPMODES == 1
      Serial.print("GH_SetMode / switch / opMode:");
      Serial.println(MODE);
      Serial.print("SystemMain.opmode=");
      Serial.println(SystemMain.opMode);
      Serial.print("SystemMain.waitTime=");
      Serial.println(SystemMain.waitTime);
#endif
      break;
    case 1:
      SystemMain.opMode = 1;
      SystemMain.waitTime = 2000;
#if DEBUG_MODE_OPMODES == 1
      Serial.print("GH_SetMode / switch / opMode:");
      Serial.println(MODE);
      Serial.print("SystemMain.opmode=");
      Serial.println(SystemMain.opMode);
      Serial.print("SystemMain.waitTime=");
      Serial.println(SystemMain.waitTime);
#endif

      break;
    case 2:
      SystemMain.opMode = 2;
      SystemMain.waitTime = 1000;
#if DEBUG_MODE_OPMODES == 1
      Serial.print("GH_SetMode / switch / opMode:");
      Serial.println(MODE);
      Serial.print("SystemMain.opmode=");
      Serial.println(SystemMain.opMode);
      Serial.print("SystemMain.waitTime=");
      Serial.println(SystemMain.waitTime);
#endif

      break;
    default:

      break;
  }
#if DEBUG_MODE_OPMODES == 1
  Serial.println("End GH_setMode(MODE)");
#endif
}
