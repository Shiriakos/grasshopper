#include <GH_Player.h>

Player::Player(int16_t _x_coordinate, int16_t _y_coordinate, int16_t _wide,
               int16_t _high, uint16_t _colour, uint16_t _speed,
               bool _move_control, int16_t _x_left_limit,
               int16_t _x_right_limit, int16_t _y_up_limit,
               int16_t _y_down_limit) {
  x_coordinate = _x_coordinate;
  y_coordinate = _y_coordinate;
  wide = _wide;
  high = _high;
  colour = _colour;
  speed = _speed;
  move_control = _move_control;
  x_left_limit = _x_left_limit;
  x_right_limit = _x_right_limit;
  y_up_limit = _y_up_limit;
  y_down_limit = _y_down_limit;
}

void Player::move() {
    if (GH_esp32BT::available()) {
    switch (GH_esp32BT::read()) {
      case 1:  // RIGHT
        if (x_coordinate <= x_right_limit) {
          x_coordinate = x_coordinate + 20;
          GH_Display::fillRect(x_coordinate, y_coordinate, 20, 20, WHITE);
          GH_Display::fillRect(x_coordinate - 20, y_coordinate, 20, 20, BLACK);
        }
        break;
      case 2:  // LEFT
        if (x_coordinate >= x_left_limit) {
          x_coordinate = x_coordinate - 20;
          GH_Display::fillRect(x_coordinate, y_coordinate, 20, 20, WHITE);
          GH_Display::fillRect(x_coordinate + 20, y_coordinate, 20, 20, BLACK);
        }
        break;
      case 3:  // UP
        if (y_coordinate >= y_down_limit) {
          y_coordinate = y_coordinate - 20;
          GH_Display::fillRect(x_coordinate, y_coordinate, 20, 20, WHITE);
          if (y_coordinate <= y_up_limit + 20) {
            GH_Display::fillRect(x_coordinate, y_coordinate + 20, 20, 20,
                                 BLACK);
          }
        }
        break;
      case 4:  // DOWN
        if (y_coordinate <= y_up_limit) {
          y_coordinate = y_coordinate + 20;
          GH_Display::fillRect(x_coordinate, y_coordinate, 20, 20, WHITE);
          if (y_coordinate >= y_down_limit - 20) {
            GH_Display::fillRect(x_coordinate, y_coordinate - 20, 20, 20,
                                 BLACK);
          }
        }
        break;
      default:
        break;
    }
  }
}

uint16_t Player::get_speed() { return speed; }

uint16_t Player::get_x() { return x_coordinate; }

uint16_t Player::get_y() { return y_coordinate; }
