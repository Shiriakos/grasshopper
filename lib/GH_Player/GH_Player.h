#include <Arduino.h>
#include <GH_Display.h>
#include <GH_ESP32BT.h>

#ifndef _GH_PLAYER_
#define _GH_PLAYER_

#pragma once

class Player {
 private:
  int16_t x_coordinate;
  int16_t y_coordinate;
  int16_t wide;
  int16_t high;
  uint16_t colour;
  uint16_t speed;
  int16_t x_left_limit;
  int16_t x_right_limit;
  int16_t y_up_limit;
  int16_t y_down_limit;

 public:
  bool move_control;
  Player(int16_t _x_coordinate, int16_t _y_coordinate, int16_t _wide,
         int16_t _high, uint16_t _colour, uint16_t _speed, bool _move_control,
         int16_t _x_left_limit, int16_t _x_right_limit, int16_t _y_up_limit,
         int16_t _y_down_limit);

  void move();
  uint16_t get_speed();
  uint16_t get_x();
  uint16_t get_y();
};

#endif
